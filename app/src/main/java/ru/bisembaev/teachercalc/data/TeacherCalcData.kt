package ru.bisembaev.teachercalc.data

import android.databinding.BaseObservable
import android.databinding.Bindable
import ru.bisembaev.teachercalc.BR

data class TeacherCalcData(

        var _studentsCount: Int = 0,
        var _fivesCount: Int = 0,
        var _foursCount: Int = 0,
        var _triplesCount: Int = 0,
        var _twosCount: Int = 0,
        var _notAttCount: Int = 0

) : BaseObservable() {

    var studentsCount: Int = _studentsCount
        @Bindable get() = field
        set(value) {
            if (value >= 0) field = value
            notifyPropertyChanged(BR.studentsCount)
        }

    var fivesCount: Int = _fivesCount
        @Bindable get() = field
        set(value) {
            if (value >= 0) field = value
            notifyPropertyChanged(BR.fivesCount)
        }

    var foursCount: Int = _foursCount
        @Bindable get() = field
        set(value) {
            if (value >= 0) field = value
            notifyPropertyChanged(BR.foursCount)
        }

    var triplesCount: Int = _triplesCount
        @Bindable get() = field
        set(value) {
            if (value >= 0) field = value
            notifyPropertyChanged(BR.triplesCount)
        }

    var twosCount: Int = _twosCount
        @Bindable get() = field
        set(value) {
            if (value >= 0) field = value
            notifyPropertyChanged(BR.twosCount)
        }

    var notAttCount: Int = _notAttCount
        @Bindable get() = field
        set(value) {
            if (value >= 0) {
                field = value
                notifyPropertyChanged(BR.notAttCount)
            }
        }

    override fun toString(): String {
        return "TeacherCalcData(studentsCount=$studentsCount, " +
                "fivesCount=$fivesCount, " +
                "foursCount=$foursCount, " +
                "triplesCount=$triplesCount, " +
                "twosCount=$twosCount, " +
                "notAttCount=$notAttCount)"
    }


}
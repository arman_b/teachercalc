package ru.bisembaev.teachercalc.util

import ru.bisembaev.teachercalc.data.TeacherCalcData

class Calculator(val data: TeacherCalcData) {

    fun isCorrectData(): Boolean {
        return (
                data.studentsCount == (data.fivesCount + data.foursCount + data.triplesCount +
                data.twosCount + data.notAttCount)
        )
    }

    fun isZeroData(): Boolean {
        return (
                data.studentsCount == 0 && data.fivesCount == 0 && data.foursCount == 0
                        && data.triplesCount == 0 && data.twosCount == 0 && data.notAttCount == 0
        )
    }

    fun academicPerformance(): Double {
        if (isCorrectData()) {
            //Успеваемость = (кол-во "5" + кол-во "4" + "кол-во "3") / общее количество учащихся
            return (data.fivesCount + data.foursCount + data.triplesCount) /
                    data.studentsCount.toDouble()
        } else return 0.0
    }

    fun qualityOfKnowledge(): Double {
        if (isCorrectData()) {
            //Качество знаний = (кол-во "5" + кол-во "4") / общее количество учащихся
            return (data.fivesCount + data.foursCount) / data.studentsCount.toDouble()
        } else return 0.0
    }

    fun trained(): Double {
        if (isCorrectData()) {
            //Обученность = (кол-во "5" + кол-во "4" * 0,64 + кол-во "3" * 0,36 + кол-во "2" * 0,16
            // + кол-во "н/а" * 0,08 ) / общее количество учащихся
            return (
                    data.fivesCount +
                            data.foursCount * 0.64 +
                            data.triplesCount * 0.36 +
                            data.twosCount * 0.16 +
                            data.notAttCount * 0.08) /
                    data.studentsCount.toDouble()
        } else return 0.0
    }

    fun average(): Double {
        if (isCorrectData()) {
            return (
                    (
                        data.fivesCount * 5 +
                        data.foursCount * 4 +
                        data.triplesCount * 3 +
                        data.twosCount * 2
                    ) / (
                        data.fivesCount + data.foursCount + data.triplesCount + data.twosCount
                                + data.notAttCount
                    ).toDouble()
            )
        } else return 0.0
    }

}
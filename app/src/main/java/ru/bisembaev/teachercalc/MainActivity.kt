package ru.bisembaev.teachercalc

import android.databinding.DataBindingUtil
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.BottomSheetBehavior
import android.text.Editable
import android.text.TextWatcher
import android.view.Menu
import ru.bisembaev.teachercalc.data.TeacherCalcData
import ru.bisembaev.teachercalc.databinding.ActivityMainBinding
import ru.bisembaev.teachercalc.help.Logger
import ru.bisembaev.teachercalc.help.showToast
import ru.bisembaev.teachercalc.util.Calculator
import android.content.Intent



class MainActivity : AppCompatActivity() {

    private val logTag : String = this.javaClass.simpleName

    private lateinit var b: ActivityMainBinding
    private lateinit var calculator: Calculator
    private val logger: Logger = Logger(true)
    private val teacherCalcData: TeacherCalcData = TeacherCalcData()

    override fun onCreate(savedInstanceState: Bundle?) {
        logger.log(logTag, "onCreate()")
        super.onCreate(savedInstanceState)
        b = DataBindingUtil.setContentView(this, R.layout.activity_main)
        b.includeToolbar?.toolbar?.title = resources.getString(R.string.app_name)
        setSupportActionBar(b.includeToolbar?.toolbar)

        logger.log(logTag, teacherCalcData.toString())
        b.teacherCalcData = teacherCalcData

        b.calculateButton.setOnClickListener {
            logger.log(logTag, teacherCalcData.toString())
            calculator = Calculator(teacherCalcData)
            if (!calculator.isCorrectData()) {
                this.showToast(resources.getString(R.string.message_not_correct_data))
            } else if (calculator.isZeroData()) {
                this.showToast(resources.getString(R.string.message_zero_data))
            } else {
                logger.log(logTag, "academicPerformance: ${calculator.academicPerformance()}")
                logger.log(logTag, "qualityOfKnowledge: ${calculator.qualityOfKnowledge()}")
                logger.log(logTag, "trained: ${calculator.trained()}")
                logger.log(logTag, "average: ${calculator.average()}")

                b.bottomSheet?.academicPerformance?.text =
                        "${"%.2f".format((calculator.academicPerformance() * 100))} %"
                b.bottomSheet?.qualityOfKnowledge?.text =
                        "${"%.2f".format((calculator.qualityOfKnowledge() * 100))} %"
                b.bottomSheet?.trained?.text = "${"%.2f".format((calculator.trained() * 100))} %"
                b.bottomSheet?.average?.text = "%.2f".format(calculator.average())

                BottomSheetBehavior.from(b.bottomSheet?.root).state =
                        BottomSheetBehavior.STATE_EXPANDED
            }
        }

        b.clearButton.setOnClickListener {
            teacherCalcData.studentsCount = 0
            teacherCalcData.fivesCount = 0
            teacherCalcData.twosCount = 0
            teacherCalcData.notAttCount = 0
            teacherCalcData.triplesCount = 0
            teacherCalcData.foursCount = 0
            logger.log(logTag, teacherCalcData.toString())
        }

        b.studentsCountPlus1.setOnClickListener {
            teacherCalcData.studentsCount++
        }

        b.studentsCountNeg1.setOnClickListener {
            teacherCalcData.studentsCount--
        }

        b.fivesPlus1.setOnClickListener {
            teacherCalcData.fivesCount++
        }

        b.fivesNeg1.setOnClickListener {
            teacherCalcData.fivesCount--
        }

        b.fourPlus1.setOnClickListener {
            teacherCalcData.foursCount++
        }

        b.fourNeg1.setOnClickListener {
            teacherCalcData.foursCount--
        }

        b.triplePlus1.setOnClickListener {
            teacherCalcData.triplesCount++
        }

        b.tripleNeg1.setOnClickListener {
            teacherCalcData.triplesCount--
        }

        b.twoPlus1.setOnClickListener {
            teacherCalcData.twosCount++
        }

        b.twoNeg1.setOnClickListener {
            teacherCalcData.twosCount--
        }

        b.notAtPlus1.setOnClickListener {
            teacherCalcData.notAttCount++
        }

        b.notAtNeg1.setOnClickListener {
            teacherCalcData.notAttCount--
        }

        b.editStudentsCount.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
            override fun afterTextChanged(p0: Editable?) {
                if (p0.toString().isEmpty()) teacherCalcData.studentsCount = 0
                b.editStudentsCount.setSelection(b.editStudentsCount.text.length)
            }
        })

        b.editFivesCount.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
            override fun afterTextChanged(p0: Editable?) {
                if (p0.toString().isEmpty()) teacherCalcData.fivesCount = 0
                b.editFivesCount.setSelection(b.editFivesCount.text.length)
            }
        })

        b.editFourCount.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
            override fun afterTextChanged(p0: Editable?) {
                if (p0.toString().isEmpty()) teacherCalcData.foursCount = 0
                b.editFourCount.setSelection(b.editFourCount.text.length)
            }
        })

        b.editNotAtCount.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
            override fun afterTextChanged(p0: Editable?) {
                if (p0.toString().isEmpty()) teacherCalcData.notAttCount = 0
                b.editNotAtCount.setSelection(b.editNotAtCount.text.length)
            }
        })

        b.editTripleCount.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
            override fun afterTextChanged(p0: Editable?) {
                if (p0.toString().isEmpty()) teacherCalcData.triplesCount = 0
                b.editTripleCount.setSelection(b.editTripleCount.text.length)
            }
        })

        b.editTwoCount.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
            override fun afterTextChanged(p0: Editable?) {
                if (p0.toString().isEmpty()) teacherCalcData.twosCount = 0
                b.editTwoCount.setSelection(b.editTwoCount.text.length)
            }
        })

        b.bottomSheet?.closeBottomSheet?.setOnClickListener({
            logger.log(logTag, "closeBottomSheet")
            BottomSheetBehavior.from(b.bottomSheet?.root).state =
                    BottomSheetBehavior.STATE_COLLAPSED
        })

        b.bottomSheet?.shareResult?.setOnClickListener({
            logger.log(logTag, "shareResult")
            val sendIntent = Intent()
            sendIntent.action = Intent.ACTION_SEND

            sendIntent.putExtra(Intent.EXTRA_TEXT,
                    resources.getText(R.string.label_academic_performance).toString() +
                    " ${"%.2f".format((calculator.academicPerformance() * 100))} %" + "\n" +
                    resources.getText(R.string.label_quality_of_knowledge).toString() +
                    " ${"%.2f".format((calculator.qualityOfKnowledge() * 100))} %" + "\n" +
                    resources.getText(R.string.label_trained).toString() +
                    " ${"%.2f".format((calculator.trained() * 100))} %" + "\n" +
                    resources.getText(R.string.label_average).toString() +
                    " ${"%.2f".format((calculator.average() * 100))} %" + "\n"
            )

            sendIntent.type = "text/plain"
            startActivity(Intent.createChooser(sendIntent, resources.getText(R.string.share_title)))
        })

    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        super.onCreateOptionsMenu(menu)
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

}
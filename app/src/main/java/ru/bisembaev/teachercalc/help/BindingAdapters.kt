package ru.bisembaev.teachercalc.help

import android.databinding.BindingAdapter
import android.widget.EditText

@BindingAdapter("fromInt")
fun EditText.fromInt(int : Int) {
    if (int != 0)
    setText(int.toString())
}
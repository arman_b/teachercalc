package ru.bisembaev.teachercalc.help

import android.util.Log

class Logger(var enabled : Boolean) {

    private val loggerTag : String = "Logger :: "

    /**
     * Logging message with DEBUG
     * @param logTag
     * @param message
     */
    fun log(logTag: String, message: String) {
        if (enabled) Log.d(loggerTag + logTag, message)
    }

}